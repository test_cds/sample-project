package com.example.sampleapplication;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class UnitTestingTest {

    UnitTesting unitTesting;
    @Before
    public void init() {
        unitTesting = new UnitTesting();
    }
    @Test
    public void textWhenInputTrue()
    {
        boolean result=unitTesting.testUnit( true);
        Assert.assertTrue(result);
    }
    @Test
    public void testWhenInputFalse()
    {
        boolean result=unitTesting.testUnit( false);
        Assert.assertFalse( result);
    }
}