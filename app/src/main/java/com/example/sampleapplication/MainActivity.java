package com.example.sampleapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private Button button;
    private TextView textView;
    private Boolean shouldTextBeShown=true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button=findViewById(R.id.clickMeButton);
        textView=findViewById(R.id.textView);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                performClickAction();
            }
        });
    }
    private void performClickAction()
    {
        if(shouldTextBeShown)
        {
            textView.setVisibility(View.VISIBLE);
            shouldTextBeShown=false;
            button.setText(R.string.hide_label_text);
        }
        else
        {
            shouldTextBeShown=true;
            textView.setVisibility(View.GONE);
            button.setText(R.string.click_me);

        }

    }

}